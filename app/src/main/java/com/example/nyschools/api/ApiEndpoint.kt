package com.example.nyschools.api


import com.example.nyschools.model.ResponseModel
import retrofit2.Response
import retrofit2.http.GET


interface ApiEndpoint {
    @GET("s3k6-pzi2.json")
    suspend fun getNySchollData():Response<ResponseModel>

}