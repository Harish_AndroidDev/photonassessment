package com.example.nyschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler
import com.example.nyschools.databinding.RecyclerLayoutBinding
import com.example.nyschools.model.NYSchoolResponseModel

class DataAdapter:RecyclerView.Adapter<DataAdapter.MyViewHolder>() {
    private var items = ArrayList<NYSchoolResponseModel>()
    fun setData(data:ArrayList<NYSchoolResponseModel>){
        this.items = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataAdapter.MyViewHolder {
       val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecyclerLayoutBinding.inflate(layoutInflater)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DataAdapter.MyViewHolder, position: Int) {
        holder.bind(items[position])

    }


    override fun getItemCount(): Int {
       return items.size
    }
    class MyViewHolder(val binding:RecyclerLayoutBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(data:NYSchoolResponseModel){
            binding.text1.setText(data.dbn)
            binding.text2.setText(data.schoolName)
            binding.executePendingBindings()
        }
    }
}