package com.example.nyschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nyschools.databinding.FragmentMainBinding
import com.example.nyschools.viewmodel.MainViewModel
import com.example.nyschools.viewmodel.MainViewModelFactory

class MainFragment:Fragment() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var _binding: FragmentMainBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater,container,false)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        mainViewModel = ViewModelProvider(this, MainViewModelFactory())[MainViewModel::class.java]
        _binding.executePendingBindings()
        _binding.recycler.apply {
            layoutManager = LinearLayoutManager(requireContext())
            val deacration = DividerItemDecoration(requireContext(),LinearLayoutManager.VERTICAL)
            addItemDecoration(deacration)
        }
        initObserver()

        }
        private fun initObserver(){
            mainViewModel.responseData.observe(viewLifecycleOwner, Observer {
                result ->
                when (result){
                    is Result.Success -> {
                        mainViewModel.setAdapterData(result.data.items)
                    }
                    is Result.Error ->{

                    }
                    is Result.ErrorException ->{

                    }
                }
            })
        }
    }

