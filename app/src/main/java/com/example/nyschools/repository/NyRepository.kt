package com.example.nyschools.repository

import com.example.nyschools.api.ApiEndpoint
import com.example.nyschools.api.RetrofitClient

class NyRepository {
    private val retrofit = RetrofitClient.getRetrofitInstance().create(ApiEndpoint::class.java)
    suspend fun getRepository() =  retrofit.getNySchollData()
}