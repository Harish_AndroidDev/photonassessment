package com.example.nyschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nyschools.adapter.DataAdapter
import com.example.nyschools.model.NYSchoolResponseModel
import com.example.nyschools.model.ResponseModel
import com.example.nyschools.repository.NyRepository
import kotlinx.coroutines.launch
import com.example.nyschools.ui.Result

class MainViewModel(private val repository: NyRepository):ViewModel() {

    private val nyResponse = MutableLiveData<Result<ResponseModel>>()
    val responseData: LiveData<Result<ResponseModel>> = nyResponse
    private var dataAdapter: DataAdapter = DataAdapter()
    init {
        makeApiCall()
    }
    fun getAdapter(): DataAdapter{
        return dataAdapter
    }
    fun setAdapterData(data:ArrayList<NYSchoolResponseModel>) {
        dataAdapter.setData(data)
        dataAdapter.notifyDataSetChanged()
    }
    private fun makeApiCall() = viewModelScope.launch {
        try {
            val response = repository.getRepository()
            if(response.isSuccessful){
                nyResponse.value = Result.Success(response.body()!!)
            }
            else{
                nyResponse.value = Result.Error(response.message())
            }

        }
        catch (e:Exception){
            nyResponse.value = Result.ErrorException(e)
        }

    }

}