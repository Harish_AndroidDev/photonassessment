package com.example.nyschools.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nyschools.repository.NyRepository
import kotlin.jvm.Throws

class MainViewModelFactory:ViewModelProvider.Factory {
    @Throws(IllegalArgumentException::class)
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(repository = NyRepository()) as T
        throw IllegalArgumentException("unknownViewmodelClass")
    }
}